package anosym.urlbuilder;

import java.util.List;

import com.google.common.collect.ImmutableList;

import anosym.urlbuilder.AbsolutePathWithQueryParameterBuilder;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 11:52:17 AM
 */
public class AbsolutePathWithQueryParameterBuilderTest {

  @Test
  public void testAbsolutePathWithoutScheme() {
    final String absolutePath = "www.afrocoin.com/relative-url/?pmm=838383";
    final String expectedAbsolutePath = "http://www.afrocoin.com/relative-url/?pmm=838383";
    assertThrows(IllegalArgumentException.class, () -> new AbsolutePathWithQueryParameterBuilder(absolutePath).build());
  }

  @Test
  public void testAbsolutePathWithScheme() {
    final String absolutePath = "https://www.afrocoin.com/relative-url?pmm=838383";
    final String actualAbsolutePath = new AbsolutePathWithQueryParameterBuilder(absolutePath).build();
    assertThat(actualAbsolutePath, is(absolutePath));
  }

  @Test
  public void testAbsolutePathWithPort() {
    final String absolutePath = "https://www.afrocoin.com:9090/relative-url?pmm=838383";
    final String actualAbsolutePath = new AbsolutePathWithQueryParameterBuilder(absolutePath).build();
    assertThat(actualAbsolutePath, is(absolutePath));
  }

  @Test
  public void testAbsolutePathWithAdditionalPathSegmentsWithEmptyContextPath() {
    final String expectedAbsolutePath = "https://www.afrocoin.com:9090/additional?pmm=838383";
    final String absolutePath = "https://www.afrocoin.com:9090?pmm=838383";
    final String actualAbsolutePath = new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendPathSegment("additional")
            .build();
    assertThat(actualAbsolutePath, is(expectedAbsolutePath));
  }

  @Test
  public void testAbsolutePathWithAdditionalPathSegmentsWithContextPath() {
    final String expectedAbsolutePath = "https://www.afrocoin.com:9090/relative-url/additional?pmm=838383";
    final String absolutePath = "https://www.afrocoin.com:9090/relative-url?pmm=838383";
    final String actualAbsolutePath = new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendPathSegment("additional")
            .build();
    assertThat(actualAbsolutePath, is(expectedAbsolutePath));
  }

  @Test
  public void testAbsolutePathWithExcludeQueryParameters() {
    final String expectedAbsolutePath = "https://www.afrocoin.com:9090/relative-url/additional?pmm=838383";
    final String absolutePath = "https://www.afrocoin.com:9090/relative-url/additional?pmm=838383&kk=83833&hh=83833737";
    final List<String> paramatersToRemove = ImmutableList.of("kk", "hh");
    final String actualAbsolutePath = new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .removeQueryParameter((qp) -> paramatersToRemove.contains(qp))
            .build();
    assertThat(actualAbsolutePath, is(expectedAbsolutePath));
  }

}
