package anosym.urlbuilder;

import org.junit.jupiter.api.Test;

import anosym.urlbuilder.PathWithQueryParameterBuilder;
import anosym.urlbuilder.RelativePathWithQueryParameterBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 3:13:36 AM
 */
public class RelativePathWithQueryParameterBuilderTest {

  @Test
  public void testNormalRelativePath() {
    final String path = "/relative-path";
    final PathWithQueryParameterBuilder pathWithQueryParameterBuilder
            = new RelativePathWithQueryParameterBuilder(path);
    final String actualPath = pathWithQueryParameterBuilder.build();
    assertThat(actualPath, is(path));
  }

  @Test
  public void testNormalRelativePathWithQuery() {
    final String path = "/relative-path?param1=3434&param2=jsuu7347";
    final PathWithQueryParameterBuilder pathWithQueryParameterBuilder
            = new RelativePathWithQueryParameterBuilder(path);
    final String actualPath = pathWithQueryParameterBuilder.build();
    assertThat(actualPath, is(path));
  }

  @Test
  public void testNormalRelativePathWithEncodedQuery() {
    final String path = "/relative-path?param1=3434&param2=jsuu7347&param3=j-%26-b";
    final PathWithQueryParameterBuilder pathWithQueryParameterBuilder
            = new RelativePathWithQueryParameterBuilder(path);
    final String actualPath = pathWithQueryParameterBuilder.build();
    assertThat(actualPath, is(path));
  }

  @Test
  public void testNormalRelativePathWithQueryPlusAdditionalParameters() {
    final String path = "/relative-path?param1=3434&param2=jsuu7347";
    final PathWithQueryParameterBuilder pathWithQueryParameterBuilder
            = new RelativePathWithQueryParameterBuilder(path);
    final String actualPath = pathWithQueryParameterBuilder.appendQueryParameter("param3", "734438").build();
    final String expectedPath = "/relative-path?param1=3434&param2=jsuu7347&param3=734438";
    assertThat(actualPath, is(expectedPath));
  }

  @Test
  public void testPathWithAdditionalPathSegmentsWithEmptyContextPath() {
    final String expectedPath = "/additional?pmm=838383";
    final String Path = "/?pmm=838383";
    final String actualPath = new RelativePathWithQueryParameterBuilder(Path)
            .appendPathSegment("additional")
            .build();
    assertThat(actualPath, is(expectedPath));
  }

  @Test
  public void testPathWithAdditionalPathSegmentsWithContextPath() {
    final String expectedPath = "/relative-url/additional?pmm=838383";
    final String Path = "/relative-url?pmm=838383";
    final String actualPath = new RelativePathWithQueryParameterBuilder(Path)
            .appendPathSegment("additional")
            .build();
    assertThat(actualPath, is(expectedPath));
  }

}
