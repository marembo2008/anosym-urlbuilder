package anosym.sslcontext;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Dec 16, 2015, 11:35:30 PM
 */
@ApplicationScoped
public class SslContextService {

  @Inject
  private Instance<HttpServletRequest> servletRequest;

  public boolean isSecure() {
    /**
     * The Amazon Elastic Load Balancer (ELB) supports a HTTP header called X-FORWARDED-PROTO. All the HTTPS requests
     * going through the ELB will have the value of X-FORWARDED-PROTO equal to “HTTPS”. For the HTTP requests, we can
     * force HTTPS by adding a simple rewrite rule.
     */
    final String xForwardedProto = servletRequest.get().getHeader("X-FORWARDED-PROTO");
    final String scheme = servletRequest.get().getScheme();
    return "https".equals(scheme) || "https".equals(xForwardedProto);
  }

}
