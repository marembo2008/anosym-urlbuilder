package anosym.urlbuilder;

import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import anosym.sslcontext.SslContextService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;

import static java.lang.String.format;
import static anosym.urlbuilder.PathUrlBuilderHelper.sanitize;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 3:41:56 AM
 */
@ApplicationScoped
class AbsoluteUrlBuilderImpl implements AbsoluteUrlBuilder {

  @Inject
  private RelativeUrlBuilder relativeUrlBuilder;

  @Inject
  private HttpServletRequest servletRequest;

  @Inject
  private SslContextService sslContextService;

  @Nonnull
  @Override
  public String buildWithCurrentPath() {
    return format("%s%s", getApplicationUrl(), relativeUrlBuilder.buildWithCurrentPath());
  }

  @Nonnull
  @Override
  public String buildWithCurrentPath(@Nonnull final Map<String, String> queryParameters) {
    checkNotNull(queryParameters, "The queryParameters must not be null");

    final String relativeUrl = relativeUrlBuilder.buildWithCurrentPath(queryParameters);
    return format("%s%s", getApplicationUrl(), relativeUrl);
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter() {
    return format("%s%s", getApplicationUrl(), relativeUrlBuilder.buildWithCurrentPathAndCurrentQueryParameter());
  }

  @Nonnull
  @Override
  public String buildSecureWithCurrentPathAndCurrentQueryParameter() {
    return format("%s%s", getApplicationUrl(true), relativeUrlBuilder.buildWithCurrentPathAndCurrentQueryParameter());
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters) {
    checkNotNull(queryParameters, "The queryParameters must not be null");

    final String relativePath = relativeUrlBuilder.buildWithCurrentPathAndCurrentQueryParameter(queryParameters);
    return format("%s%s", getApplicationUrl(), relativePath);
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath) {
    checkNotNull(relativePath, "The relativePath must not be null");

    return format("%s%s", getApplicationUrl(), sanitize(relativePath));
  }

  @Nonnull
  @Override
  public String buildWithHost(@Nonnull final String hostname) {
    checkNotNull(hostname, "The hostname must not be null");

    final String relativePath = relativeUrlBuilder.buildWithCurrentPathAndCurrentQueryParameter();
    return format("%s%s", getApplicationUrl(hostname), sanitize(relativePath));
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath, @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    final String relativeUrl = relativeUrlBuilder.buildWithPath(relativePath, queryParameters);
    return format("%s%s", getApplicationUrl(), relativeUrl);
  }

  @Nonnull
  @Override
  public String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath) {
    checkNotNull(relativePath, "The relativePath must not be null");

    final String relativeUrl = relativeUrlBuilder.buildWithPathAndCurrentQueryParameter(relativePath);
    return format("%s%s", getApplicationUrl(), relativeUrl);
  }

  @Nonnull
  @Override
  public String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath,
                                                      @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    final String relativeUrl
            = relativeUrlBuilder.buildWithPathAndCurrentQueryParameter(relativePath, queryParameters);
    return format("%s%s", getApplicationUrl(), relativeUrl);
  }

  @Nonnull
  @Override
  public String buildWithAbsolutePath(@Nonnull final String absolutePath,
                                      @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(absolutePath, "The absolutePath must not be null");

    checkNotNull(queryParameters, "The queryParameters must not be null");
    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithAbsolutePath(@Nonnull final String absolutePath,
                                      @Nonnull final String additionalPathSegment,
                                      @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(absolutePath, "The absolutePath must not be null");
    checkNotNull(additionalPathSegment, "The additionalPathSegment must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendQueryParameters(queryParameters)
            .appendPathSegment(additionalPathSegment)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithAbsolutePath(@Nonnull final String absolutePath,
                                      @Nonnull final List<String> additionalPathSegments,
                                      @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(absolutePath, "The absolutePath must not be null");
    checkNotNull(additionalPathSegments, "The additionalPathSegments must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendQueryParameters(queryParameters)
            .appendPathSegment(additionalPathSegments)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithoutQueryParameters(@Nonnull final String absolutePath,
                                            @Nonnull final String... queryParametersToExclude) {
    checkNotNull(absolutePath, "The absolutePath must not be null");
    checkNotNull(queryParametersToExclude, "The queryParametersToExclude must not be null");

    final List<String> queryParameters = ImmutableList.copyOf(queryParametersToExclude);
    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .removeQueryParameter((qp) -> queryParameters.contains(qp))
            .build();
  }

  @Nonnull
  @Override
  public String buildWithAbsolutePathAndCurrentQueryParameter(@Nonnull final String absolutePath) {
    checkNotNull(absolutePath, "The absolutePath must not be null");

    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendQueryParameters(expandCurrentQueryParameters())
            .build();
  }

  @Nonnull
  @Override
  public String buildWithAbsolutePathAndCurrentQueryParameter(@Nonnull final String absolutePath,
                                                              @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(absolutePath, "The absolutePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new AbsolutePathWithQueryParameterBuilder(absolutePath)
            .appendQueryParameters(expandCurrentQueryParameters())
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  private String getApplicationUrl() {
    final String host = servletRequest.getHeader("Host");
    return getApplicationUrl(host);
  }

  @Nonnull
  private String getApplicationUrl(final boolean secure) {
    final String host = servletRequest.getHeader("Host");
    return getApplicationUrl(host, secure);
  }

  @Nonnull
  private String getApplicationUrl(@Nonnull final String host) {
    return getApplicationUrl(host, sslContextService.isSecure());
  }

  @Nonnull
  private String getApplicationUrl(@Nonnull final String host, final boolean secure) {
    final String httpProtocol = secure ? "https" : "http";
    return format("%s://%s", httpProtocol, host);
  }

  @Nonnull
  private List<ParameterNameValue> expandCurrentQueryParameters() {
    final ImmutableList.Builder<ParameterNameValue> builder = ImmutableList.builder();
    final Map<String, String[]> queryParameters = servletRequest.getParameterMap();
    queryParameters
            .keySet()
            .stream()
            .forEach((parameterName) -> {
              for (final String parameterValue : queryParameters.get(parameterName)) {
                builder.add(new ParameterNameValue(parameterName, parameterValue));
              }
            });

    return builder.build();
  }

}
