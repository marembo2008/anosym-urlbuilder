package anosym.urlbuilder;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

/**
 * Only used to build relative urls.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 1:53:32 AM
 */
public interface RelativeUrlBuilder {

    @Nonnull
    String buildWithCurrentPath();

    @Nonnull
    String buildWithCurrentPath(@Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter();

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final String ignoreParametersWithRegex);

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters,
                                                        @Nonnull final String ignoreParametersWithRegex);

    @Nonnull
    String buildWithPath(@Nonnull final String relativePath, @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithoutQueryParameters(@Nonnull final String absolutePath,
                                       @Nonnull final Predicate<String> queryParameterSelector);

    String buildWithPath(@Nonnull final String relativePath,
                         @Nonnull final String additionalPathSegment,
                         @Nonnull final Map<String, String> queryParameters);

    String buildWithPath(@Nonnull final String relativePath,
                         @Nonnull final List<String> additionalPathSegments,
                         @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithPath(@Nonnull final String relativePath, @Nonnull final QueryParameter<?>... queryParameters);

    @Nonnull
    String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath);

    @Nonnull
    String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath,
                                                 @Nonnull final Map<String, String> queryParameters);

}
