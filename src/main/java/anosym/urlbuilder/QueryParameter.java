package anosym.urlbuilder;

import javax.annotation.Nonnull;
import lombok.Builder;
import lombok.Getter;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jan 30, 2016, 6:54:53 AM
 */
@Getter
@Builder
public class QueryParameter<T> {

    @Nonnull
    private String parameter;

    @Nonnull
    private T parameterValue;

}
