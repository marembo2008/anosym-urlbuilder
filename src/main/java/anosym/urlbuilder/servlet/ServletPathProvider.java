package anosym.urlbuilder.servlet;

import javax.annotation.Nonnull;

/**
 * When a request is wrapped, it may be the case, that the current servlet path is not what the context requires.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Feb 5, 2017, 4:47:29 PM
 */
public interface ServletPathProvider {

    /**
     * Returns the current servlet path. Must not be null.
     */
    @Nonnull
    String getServletPath();

}
