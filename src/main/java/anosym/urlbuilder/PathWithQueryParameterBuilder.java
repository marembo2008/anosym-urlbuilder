package anosym.urlbuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import javax.annotation.Nonnull;

import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 2:59:40 AM
 */
abstract class PathWithQueryParameterBuilder {

    private static final Splitter QUERY_PARAMETER_SPLITTER = Splitter.on("&");

    private static final Splitter QUERY_PARAMETER_VALUE_SPLITTER = Splitter.on("=");

    private static final Splitter FORWARD_SLASH_SPLITTER = Splitter.on('/').trimResults().omitEmptyStrings();

    protected final Map<String, Set<String>> queryParameters;

    protected final List<String> additionalPathSegments;

    protected URI uri;

    public PathWithQueryParameterBuilder(@Nonnull final String pathWithPossibleQueryParameter) {
        checkNotNull(pathWithPossibleQueryParameter, "The pathWithPossibleQueryParameter must not be null");

        try {
            this.uri = new URI(pathWithPossibleQueryParameter);
        } catch (final URISyntaxException ex) {
            throw Throwables.propagate(ex);
        }

        this.queryParameters = Maps.newTreeMap();
        final String rawQuery = uri.getRawQuery();
        if (!isNullOrEmpty(rawQuery)) {
            appendQuery(rawQuery);
        }

        this.additionalPathSegments = new ArrayList<>();
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendPathSegment(@Nonnull final String... additionalPathSegments) {
        final List<String> pathSegments = Arrays.stream(additionalPathSegments)
                .map(this::sanitizePathSegment)
                .flatMap(List::stream)
                .collect(toList());
        this.additionalPathSegments.addAll(pathSegments);
        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendPathSegment(@Nonnull final List<String> additionalPathSegments) {
        final List<String> pathSegments = additionalPathSegments
                .stream()
                .map(this::sanitizePathSegment)
                .flatMap(List::stream)
                .collect(toList());
        this.additionalPathSegments.addAll(pathSegments);
        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQuery(@Nonnull final String rawQuery) {
        checkNotNull(rawQuery, "The rawQuery must not be null");

        for (final String queryParameter : QUERY_PARAMETER_SPLITTER.split(rawQuery)) {
            final List<String> queryParameterNameValuePair = QUERY_PARAMETER_VALUE_SPLITTER
                    .splitToList(queryParameter)
                    .stream()
                    .map(this::decode)
                    .collect(toList());
            checkArgument(queryParameterNameValuePair.size() == 2,
                          "The queryParameter{%s} defines invalid name-value pair", queryParameter);

            this.queryParameters
                    .computeIfAbsent(queryParameterNameValuePair.get(0), (queryParam) -> new HashSet<>())
                    .add(queryParameterNameValuePair.get(1));
        }
        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQueryParameter(@Nonnull final String parameterName,
                                                                    @Nonnull final Object parameterValue) {
        checkNotNull(parameterName, "The parameterName must not be null");
        checkNotNull(parameterValue, "The parameterValue must not be null");

        this.queryParameters
                .computeIfAbsent(parameterName, (queryParam) -> new HashSet<>())
                .add(String.valueOf(parameterValue));
        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder removeQueryParameter(@Nonnull final Predicate<String> parameterSelector) {
        checkNotNull(parameterSelector, "The parameterSelector must not be null");

        return selectQueryParameter(parameterSelector.negate());
    }

    @Nonnull
    public final PathWithQueryParameterBuilder selectQueryParameter(@Nonnull final Predicate<String> parameterSelector) {
        checkNotNull(parameterSelector, "The parameterSelector must not be null");

        ImmutableList
                .copyOf(this.queryParameters.keySet()).stream()
                .filter((param) -> !parameterSelector.test(param))
                .forEach((param) -> {
                    this.queryParameters.remove(param);
                });

        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQueryParameters(
            @Nonnull final ParameterNameValue... parameterNameValues) {
        checkNotNull(parameterNameValues, "The parameterNameValues must not be null");

        ImmutableList
                .copyOf(parameterNameValues)
                .forEach((pnv) -> {
                    this.queryParameters
                            .computeIfAbsent(pnv.getName(), (queryParam) -> new HashSet<>())
                            .add(String.valueOf(pnv.getValue()));
                });

        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQueryParameters(
            @Nonnull final Iterable<ParameterNameValue> parameterNameValues) {
        checkNotNull(parameterNameValues, "The parameterNameValues must not be null");

        parameterNameValues.forEach((pnv) -> {
            this.queryParameters
                    .computeIfAbsent(pnv.getName(), (queryParam) -> new HashSet<>())
                    .add(String.valueOf(pnv.getValue()));
        });
        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQueryParameters(@Nonnull final Map<String, String> queryParameters) {
        checkNotNull(queryParameters, "The queryParameters must not be null");

        queryParameters
                .entrySet()
                .stream()
                .forEach((parameterNameValue) -> {
                    this.queryParameters
                            .computeIfAbsent(parameterNameValue.getKey(), (queryParam) -> new HashSet<>())
                            .add(String.valueOf(parameterNameValue.getValue()));
                });

        return this;
    }

    @Nonnull
    public final PathWithQueryParameterBuilder appendQueryParameters(@Nonnull final QueryParameter<?>... queryParameters) {
        checkNotNull(queryParameters, "The queryParameters must not be null");

        ImmutableList
                .copyOf(queryParameters)
                .stream()
                .forEach((param) -> {
                    this.queryParameters
                            .computeIfAbsent(param.getParameter(), (queryParam) -> new HashSet<>())
                            .add(String.valueOf(param.getParameterValue()));
                });

        return this;
    }

    @Nonnull
    public abstract String build();

    @Nonnull
    protected String buildQueryParameters() {
        if (queryParameters.isEmpty()) {
            return "";
        }

        return queryParameters
                .entrySet()
                .stream()
                .flatMap((queryParam) -> {
                    return queryParam
                            .getValue()
                            .stream()
                            .map(paramValue -> format("%s=%s", queryParam.getKey(), encode(paramValue)));
                })
                .collect(joining("&", "?", ""));
    }

    @Nonnull
    protected String encode(@Nonnull final String parameter) {
        try {
            return URLEncoder.encode(parameter, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nonnull
    protected String decode(@Nonnull final String parameter) {
        try {
            return URLDecoder.decode(parameter, "UTF-8");
        } catch (final UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Nonnull
    protected List<String> sanitizePathSegment(@Nonnull final String segment) {
        return FORWARD_SLASH_SPLITTER.splitToList(segment);
    }

}
