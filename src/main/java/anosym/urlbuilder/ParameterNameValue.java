package anosym.urlbuilder;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 3:35:12 AM
 */
public final class ParameterNameValue {

    private final String name;

    private final String value;

    public ParameterNameValue(@Nonnull final String name, @Nonnull final String value) {
        checkNotNull(name, "The name must not be null");
        checkNotNull(value, "The value must not be null");

        this.name = name;
        this.value = value;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nonnull
    public String getValue() {
        return value;
    }

}
