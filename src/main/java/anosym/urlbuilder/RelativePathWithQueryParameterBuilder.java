package anosym.urlbuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.base.CharMatcher;

import static java.lang.String.format;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 11:39:28 AM
 */
final class RelativePathWithQueryParameterBuilder extends PathWithQueryParameterBuilder {

    private static final CharMatcher FORWARD_SLASH = CharMatcher.is('/');

    public RelativePathWithQueryParameterBuilder(@Nonnull final String pathWithPossibleQueryParameter) {
        super(checkNotNull(pathWithPossibleQueryParameter, "The pathWithPossibleQueryParameter must not be null"));
    }

    @Nonnull
    @Override
    public String build() {
        final String defaultPath = FORWARD_SLASH.trimFrom(uri.getPath());
        final List<String> pathSegements = new ArrayList<>(additionalPathSegments);
        pathSegements.addAll(0, sanitizePathSegment(defaultPath));

        final String completePaths = pathSegements
                .stream()
                .filter((str) -> !isNullOrEmpty(str))
                .map(this::encode)
                .collect(Collectors.joining("/"));
        final String uriPath = FORWARD_SLASH.trimFrom(completePaths);

        return format("/%s%s", uriPath, buildQueryParameters());
    }

}
