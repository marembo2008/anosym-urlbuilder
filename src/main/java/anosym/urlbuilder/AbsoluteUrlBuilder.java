package anosym.urlbuilder;

import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

/**
 * Only used to build absolute urls.
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 1:53:32 AM
 */
public interface AbsoluteUrlBuilder {

    @Nonnull
    String buildWithCurrentPath();

    @Nonnull
    String buildWithCurrentPath(@Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter();

    @Nonnull
    String buildSecureWithCurrentPathAndCurrentQueryParameter();

    @Nonnull
    String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithPath(@Nonnull final String relativePath);

    @Nonnull
    String buildWithHost(@Nonnull final String hostname);

    @Nonnull
    String buildWithPath(@Nonnull final String relativePath, @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath);

    @Nonnull
    String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath,
                                                 @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithAbsolutePath(@Nonnull final String absolutePath, @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithAbsolutePath(@Nonnull final String absolutePath,
                                 @Nonnull final String additionalPathSegment,
                                 @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithoutQueryParameters(@Nonnull final String absolutePath,
                                       @Nonnull final String... queryParametersToExclude);

    @Nonnull
    String buildWithAbsolutePath(@Nonnull final String absolutePath,
                                 @Nonnull final List<String> additionalPathSegments,
                                 @Nonnull final Map<String, String> queryParameters);

    @Nonnull
    String buildWithAbsolutePathAndCurrentQueryParameter(@Nonnull final String absolutePath);

    @Nonnull
    String buildWithAbsolutePathAndCurrentQueryParameter(@Nonnull final String absolutePath,
                                                         @Nonnull final Map<String, String> queryParameters);

}
