package anosym.urlbuilder;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 12:11:02 PM
 */
final class PathUrlBuilderHelper {

    @Nonnull
    static String sanitize(@Nonnull final String path) {
        checkNotNull(path, "The path must not be null");

        return path.startsWith("/") ? path : format("/%s", path);
    }
}
