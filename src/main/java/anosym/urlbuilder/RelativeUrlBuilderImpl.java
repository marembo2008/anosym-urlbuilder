package anosym.urlbuilder;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableList;

import anosym.urlbuilder.servlet.ServletPathProvider;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;

import static anosym.urlbuilder.PathUrlBuilderHelper.sanitize;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 2:45:36 AM
 */
@ApplicationScoped
class RelativeUrlBuilderImpl implements RelativeUrlBuilder {

  @Inject
  private HttpServletRequest servletRequest;

  @Any
  @Inject
  private Instance<ServletPathProvider> servletPathProviders;

  @Nonnull
  @Override
  public String buildWithCurrentPath() {
    return ImmutableList
            .copyOf(servletPathProviders)
            .stream()
            .map(ServletPathProvider::getServletPath)
            .findFirst()
            .orElseGet(() -> servletRequest.getServletPath());
  }

  @Nonnull
  @Override
  public String buildWithCurrentPath(@Nonnull final Map<String, String> queryParameters) {
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(buildWithCurrentPath())
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter() {
    return new RelativePathWithQueryParameterBuilder(buildWithCurrentPath())
            .appendQueryParameters(expandCurrentQueryParameters())
            .build();
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final String ignoreParametersWithRegex) {
    return new RelativePathWithQueryParameterBuilder(buildWithCurrentPath())
            .appendQueryParameters(expandCurrentQueryParameters())
            .removeQueryParameter((param) -> Pattern.matches(ignoreParametersWithRegex, param))
            .build();
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters) {
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(buildWithCurrentPath())
            .appendQueryParameters(expandCurrentQueryParameters())
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithCurrentPathAndCurrentQueryParameter(@Nonnull final Map<String, String> queryParameters,
                                                             @Nonnull final String ignoreParametersWithRegex) {
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(buildWithCurrentPath())
            .appendQueryParameters(expandCurrentQueryParameters())
            .appendQueryParameters(queryParameters)
            .removeQueryParameter((param) -> Pattern.matches(ignoreParametersWithRegex, param))
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath, @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithoutQueryParameters(@Nonnull final String relativePath,
                                            @Nonnull final Predicate<String> queryParameterSelector) {
    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .removeQueryParameter(queryParameterSelector)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath,
                              @Nonnull final String additionalPathSegment,
                              @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(additionalPathSegment, "The additionalPathSegment must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(queryParameters)
            .appendPathSegment(additionalPathSegment)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath,
                              @Nonnull final List<String> additionalPathSegments,
                              @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(additionalPathSegments, "The additionalPathSegments must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(queryParameters)
            .appendPathSegment(additionalPathSegments)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPath(@Nonnull final String relativePath, @Nonnull final QueryParameter<?>... queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath) {
    checkNotNull(relativePath, "The relativePath must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(expandCurrentQueryParameters())
            .build();
  }

  @Nonnull
  @Override
  public String buildWithPathAndCurrentQueryParameter(@Nonnull final String relativePath,
                                                      @Nonnull final Map<String, String> queryParameters) {
    checkNotNull(relativePath, "The relativePath must not be null");
    checkNotNull(queryParameters, "The queryParameters must not be null");

    return new RelativePathWithQueryParameterBuilder(sanitize(relativePath))
            .appendQueryParameters(expandCurrentQueryParameters())
            .appendQueryParameters(queryParameters)
            .build();
  }

  @Nonnull
  private List<ParameterNameValue> expandCurrentQueryParameters() {
    final ImmutableList.Builder<ParameterNameValue> builder = ImmutableList.builder();
    final Map<String, String[]> queryParameters = servletRequest.getParameterMap();
    queryParameters
            .keySet()
            .stream()
            .forEach((parameterName) -> {
              for (final String parameterValue : queryParameters.get(parameterName)) {
                builder.add(new ParameterNameValue(parameterName, parameterValue));
              }
            });

    return builder.build();
  }

}
