package anosym.urlbuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import com.google.common.base.CharMatcher;

import static java.lang.String.format;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since May 17, 2015, 11:37:28 AM
 */
class AbsolutePathWithQueryParameterBuilder extends PathWithQueryParameterBuilder {

    private static final CharMatcher FORWARD_SLASH = CharMatcher.is('/');

    public AbsolutePathWithQueryParameterBuilder(@Nonnull final String pathWithPossibleQueryParameter) {
        super(checkNotNull(pathWithPossibleQueryParameter, "The pathWithPossibleQueryParameter must not be null"));
        checkArgument(uri.getScheme() != null, "An absolute uri must provide scheme specific parts");
    }

    @Nonnull
    @Override
    public String build() {
        final String scheme = checkNotNull(uri.getScheme(), "The uri.getScheme() must not be null");
        final String host = checkNotNull(uri.getHost(), "The uri.getHost() must not be null");
        final int port = uri.getPort();
        final String defaultPath = FORWARD_SLASH.trimFrom(uri.getPath());
        final List<String> pathSegements = new ArrayList<>(additionalPathSegments);
        pathSegements.addAll(0, sanitizePathSegment(defaultPath));

        final String completePaths = pathSegements
                .stream()
                .filter((str) -> !isNullOrEmpty(str))
                .map(this::encode)
                .collect(Collectors.joining("/"));
        final String uriPath = FORWARD_SLASH.trimFrom(completePaths);
        final String queryParameter = buildQueryParameters();

        if (port > -1) {
            return format("%s://%s:%s/%s%s", scheme, host, port, uriPath, queryParameter);
        }

        return format("%s://%s/%s%s", scheme, host, uriPath, queryParameter);
    }

}
